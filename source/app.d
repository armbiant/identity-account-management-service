module source.app;

import ogm.authservice.service;
import vibe.service.main;


int main(string[] args) {
  return mainService!(AuthService)(args);
}
