module ogm.authservice.actions.createresettoken;

import vibe.data.json;

import std.datetime;
import std.exception;

version(unittest) {
  import fluent.asserts;
}

import vibeauth.collections.usercollection;
import vibeauth.data.usermodel;
import vibeauth.data.token;
import vibeauth.collections.usermemory;
import vibeauth.data.user;

/// Create a token that allows an user to change the password
struct CreateResetToken {
  /// The users email that the token will be created
  string email;

  /// Collection with the service users
  UserCollection userCollection;

  /// validation function
  void delegate(const UserModel) validation;

  ///
  Token create() {
    enforce(email != "", "The email was not set.");
    enforce(userCollection.contains(email), "The email `" ~ email ~ "` was not found.");

    auto user = userCollection[email];

    if(validation !is null) {
      validation(user.toJson.deserializeJson!UserModel);
    }

    return userCollection.createToken(email, Clock.currTime.toUTC + 2.days, [], "passwordReset");
  }
}

/// Throw an exception when the email is not set
unittest {

  auto createResetToken = CreateResetToken("");
  createResetToken.userCollection = new UserMemoryCollection([]);

  ({
    createResetToken.create();
  }).should.throwAnyException.withMessage("The email was not set.");
}

/// Throw an exception when the email does not exist
unittest {

  auto createResetToken = CreateResetToken("test@test.com");
  createResetToken.userCollection = new UserMemoryCollection([]);

  ({
    createResetToken.create();
  }).should.throwAnyException.withMessage("The email `test@test.com` was not found.");
}

/// It should create a token when the email exists
unittest {
  auto collection = new UserMemoryCollection([]);
  auto user = new User("test@test.com", "password");
  user.isActive = true;
  collection.add(user);

  auto createResetToken = CreateResetToken("test@test.com");
  createResetToken.userCollection = collection;

  auto token = createResetToken.create();

  token.name.should.not.equal("");
  token.type.should.equal("passwordReset");
  token.expire.should.be.greaterThan(Clock.currTime + 14.minutes);

  collection.byToken(token.name).email.should.equal("test@test.com");
}

/// Send the user data to a validation function if it is set
unittest {
  void mockValidation(const UserModel) {
    throw new Exception("Validation failed.");
  }

  auto collection = new UserMemoryCollection([]);
  auto user = new User("test@test.com", "password");
  user.isActive = true;
  collection.add(user);

  auto createResetToken = CreateResetToken("test@test.com");
  createResetToken.userCollection = collection;
  createResetToken.validation = &mockValidation;

  ({
    createResetToken.create();
  }).should.throwAnyException.withMessage("Validation failed.");
}

/// It should create a token for inactive users
unittest {
  auto collection = new UserMemoryCollection([]);
  auto user = new User("test@test.com", "password");
  user.isActive = false;

  collection.add(user);

  auto createResetToken = CreateResetToken("test@test.com");
  createResetToken.userCollection = collection;

  const token = createResetToken.create();

  token.name.should.not.equal("");
  token.type.should.equal("passwordReset");
  token.expire.should.be.greaterThan(Clock.currTime + 14.minutes);

  collection.byToken(token.name).email.should.equal("test@test.com");
}
