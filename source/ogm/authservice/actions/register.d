module ogm.authservice.actions.register;

import vibe.data.json;

import vibeauth.challenges.base;

import std.datetime;
import std.exception;
import std.algorithm;

import vibeauth.collections.usermemory;
import vibeauth.collections.usercollection;
import vibeauth.data.usermodel;
import vibeauth.data.token;
import vibeauth.data.user;

version(unittest) {
  import fluent.asserts;
}

/// Add a new user in the user collection
struct Register {

  /// The users salutation eg. mr/ms or unset
  string salutation;

  /// The users title eg. dr
  string title;

  /// The users first name
  string firstName;

  /// The users last name
  string lastName;

  /// The users public name
  string username;

  /// The user email
  string email;

  /// The new password
  string password;

  /// The challenge response
  string challengeResponse;

  /// Collection with the service users
  UserCollection userCollection;

  /// An user challenge like recaptcha
  IChallenge challenge;

  void delegate(const UserModel) validation;

  ///
  Token createUser() {
    enforce(email != "", "The email was not set.");
    enforce(username != "", "The username was not set.");
    enforce(firstName != "", "The first name was not set.");
    enforce(lastName != "", "The last name was not set.");
    enforce(password.length >= 10, "Your password should have at least 10 characters.");

    enforce(!userCollection.contains(email), "Email has already been taken.");
    enforce(!userCollection.contains(username), "Username has already been taken.");

    if(challenge !is null) {
      enforce(challenge.validate(challengeResponse), "Invalid challenge response.");
    }

    UserModel user;
    user.title = title;
    user.salutation = salutation;
    user.firstName = firstName;
    user.lastName = lastName;
    user.username = username;
    user.email = email;
    user.isActive = false;
    user.createdAt = Clock.currTime.toUTC;

    if(validation !is null) {
      validation(user);
    }

    userCollection.createUser(user, password);

    return userCollection.createToken(user.email, Clock.currTime.toUTC + 2.days, [], "activation");
  }
}

version(unittest) {
  class TestChallenge : IChallenge {
    string generate(HTTPServerRequest, HTTPServerResponse) {
      return "123";
    }

    bool validate(string response) {
      return response == "123";
    }

    string getTemplate(string challangeLocation) {
      return "";
    }

    Json getConfig() {
      return Json();
    }
  }
}

/// Throw an exception when the email is not set
unittest {
  auto register = Register();
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("The email was not set.");
}

/// Throw an exception when the email exists
unittest {
  auto register = Register("", "", "name", "name", "user", "test@test.com", "1234567890");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();
  register.userCollection.add(new User("test@test.com", "password"));

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("Email has already been taken.");
}

/// Throw an exception when the username exists
unittest {
  auto register = Register("", "", "name", "last", "user name", "test@test.com", "1234567890");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();
  auto user = new User("test2@test.com", "password");
  user.username = "user name";

  register.userCollection.add(user);

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("Username has already been taken.");
}

/// Throw an exception when the username is empty
unittest {
  auto register = Register("mr", "Dr.", "", "", "", "test@test.com", "1234567890");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("The username was not set.");
}

/// Throw an exception when the first name is empty
unittest {
  auto register = Register("mr", "Dr.", "", "doe", "username", "test@test.com", "1234567890");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("The first name was not set.");
}

/// Throw an exception when the last name is empty
unittest {
  auto register = Register("mr", "Dr.", "john", "", "username", "test@test.com", "1234567890");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("The last name was not set.");
}

/// Throw an exception when the password is < 9 chars
unittest {
  auto register = Register("mr", "Dr.", "name", "username", "test@test.com", "123456789");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("Your password should have at least 10 characters.");
}

/// Create an user when all data is valid
unittest {
  auto register = Register("mr", "Dr.", "name", "last name", "username", "test@test.com", "1234567890", "123");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();

  auto token = register.createUser();

  token.name.should.not.equal("");
  token.type.should.equal("activation");
  token.expire.should.be.greaterThan(Clock.currTime + 14.minutes);

  register.userCollection["test@test.com"].isValidPassword("1234567890").should.equal(true);
  register.userCollection["test@test.com"].salutation.should.equal("mr");
  register.userCollection["test@test.com"].title.should.equal("Dr.");
  register.userCollection["test@test.com"].firstName.should.equal("name");
  register.userCollection["test@test.com"].lastName.should.equal("last name");
  register.userCollection["test@test.com"].username.should.equal("username");
  register.userCollection["test@test.com"].isActive.should.equal(false);
  register.userCollection["test@test.com"].isValidToken(token.name).should.equal(true);
}

/// Send the new user data to a validation function if it is set
unittest {
  auto register = Register("mr", "Dr.","name", "last", "username", "test@test.com", "1234567890", "123");
  register.userCollection = new UserMemoryCollection([]);
  register.challenge = new TestChallenge();

  void mockValidation(const UserModel) {
    throw new Exception("Validation failed.");
  }

  register.validation = &mockValidation;

  ({
    register.createUser();
  }).should.throwAnyException.withMessage("Validation failed.");
}
