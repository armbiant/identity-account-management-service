module ogm.authservice.configuration;

import vibeauth.configuration;
import vibeauth.mail.base;

import vibe.service.configuration.general;
import vibe.service.configuration.http;
import vibe.data.json;

import std.file;
import std.path;
import std.exception;

///
struct AuthConfiguration {

  /// Configurations for all services
  GeneralConfig general;

  /// The http configuration
  HTTPConfig http;

  HTTPMQConfig httpMq;

  /// Path to the template files
  string templates;

  ///
  EmailConfig email;

  ///
  ServiceConfiguration parseOauthService() {
    ServiceConfiguration service;

    return service;
  }

  ///
  EmailConfiguration getEmailConfiguration() {
    EmailConfiguration config;

    enforce(templates.exists, "The templates folder `" ~ templates ~ "` does not exist.");

    config.activation.text = readText(buildPath(templates, "emails", "activation.txt"));
    config.activation.html = readText(buildPath(templates, "emails", "activation.html"));

    config.resetPassword.text = readText(buildPath(templates, "emails", "resetPassword.txt"));
    config.resetPassword.html = readText(buildPath(templates, "emails", "resetPassword.html"));

    config.resetPasswordConfirmation.text = readText(buildPath(templates, "emails", "resetPasswordConfirmation.txt"));
    config.resetPasswordConfirmation.html = readText(buildPath(templates, "emails", "resetPasswordConfirmation.html"));

    return config;
  }
}

///
struct EmailConfig {
  ///
  string from;
}


///
struct HTTPMQConfig {
  ///
  string url;
}