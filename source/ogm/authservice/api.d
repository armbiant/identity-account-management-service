module ogm.authservice.api;

import vibe.http.router;
import crate.http.router;
import crate.base;

import ogm.auth;
import ogm.crates.all;
import ogm.serverauthentication.auth;
import ogm.authservice.configuration;
import ogm.authservice.ajax;

import vibe.http.fileserver;

import ogm.middleware.authsession;

///
void setupAuthApi(URLRouter router, OgmCrates crates, AuthConfiguration configuration) {
  auto auth = new ServerAuthentication(crates, configuration);

  auth.bindRoutes(router);

  router
    .crateSetup
    .enable(auth.oAuth2);
}
