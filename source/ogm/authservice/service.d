module ogm.authservice.service;

import std.file;
import std.path;
import std.exception;
import std.datetime;
import std.stdio;
import std.string;

import ogm.authservice.configuration;
import ogm.authservice.api;
import ogm.crates.all;

import vibe.http.server;
import vibe.http.fileserver;
import vibe.http.router;
import vibe.db.mongo.mongo;
import vibe.core.core;
import vibe.core.log;

import vibe.service.stats;
import vibe.service.webservice;

import gis_collective.hmq.queue.mongo;
import gis_collective.hmq.queue.memory;
import gis_collective.hmq.queue.base;
import gis_collective.hmq.broadcast.base;
import gis_collective.hmq.broadcast.http;

import crate.http.router;
///
class AuthService : WebService!(AuthConfiguration) {

  private OgmCrates crates;

  void shutdown() nothrow {
  }


  IQueue!Json queueFactory(string name) nothrow @trusted {
    try {
      auto configurations = this.configuration.general.dbConfiguration;
      auto settings = configurations[0].configuration.deserializeJson!MongoClientSettings;

      auto client = connectMongoDB(settings);

      name = name.replace(".", "_");
      auto collection = client.getCollection(settings.database ~ "._hmq_auth_" ~ name);

      return new MongoQueue!Json(collection);
    } catch(Exception e) {
      return new MemoryQueue!Json();
    }
  }


  /// The main service logic
  int main() {
    scope(exit) this.statsTimer();

    auto router = new URLRouter;
    router.setupStats();
    log("Added stats routes.");

    BroadcastConfig broadcastConfig;
    broadcastConfig.hmqUrl = this.configuration.httpMq.url;
    broadcastConfig.localUrl = this.configuration.general.apiUrl;
    broadcastConfig.router = router;
    broadcastConfig.queueFactory = &queueFactory;

    auto broadcast = new HttpBroadcast(broadcastConfig);

    this.crates = this.configuration.general.dbConfiguration.setupOgmCrates;
    log("Db connection ready.");

    router.setupAuthApi(crates, this.configuration);
    log("Api setup ready.");

    auto config = this.configuration.http.toVibeConfig;
    config.serverString = this.info.name;

    logInfo("The maximum accepted request size is %s kb", config.maxRequestSize / 1024);
    logInfo("The server string is `%s`", config.serverString);

    auto l = listenHTTP(config, router);
    scope(exit) l.stopListening;

    return runEventLoop();
  }
}
