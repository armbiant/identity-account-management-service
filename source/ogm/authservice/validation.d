module ogm.authservice.validation;

import vibe.data.json;

import crate.base;
import crate.collection.memory;

import ogm.crates.all;

import std.net.isemail;
import std.exception;
import std.string;
import std.algorithm;
import std.array;

import vibeauth.collections.usercollection;
import vibeauth.data.usermodel;
import vibeauth.data.token;

version(unittest) {
  import fluent.asserts;
}

class UserDataValidation {

  private {
    Crate!Preference preferences;
  }

  ///
  this(Crate!Preference preferences) {
    this.preferences = preferences;
  }

  ///
  private string[] validEmailDomains() {
    auto range = preferences.get
      .where("name").equal("register.emailDomains")
      .and.exec;

    if(range.empty) {
      return [];
    }

    auto property = range.front;

    if("value" !in property) {
      return [];
    }

    return property["value"].to!string
      .split(",")
      .map!(a => a.strip)
      .array;
  }

  ///
  private bool isRegistrationEnabled() {
    auto range = preferences.get
      .where("name").equal("register.enabled")
      .and.exec;

    if(range.empty) {
      return false;
    }

    return range.front["value"].to!string.strip.toLower == "true";
  }

  /// validate the email domain, username and name
  void validate(const UserModel user) {
    auto email = isEmail(user.email);

    enforce(user.username.strip != "", "The provided username is invalid.");
    enforce(email.valid, "The provided email `" ~ user.email ~ "` is invalid.");

    auto domains = validEmailDomains();
    enforce(domains.length == 0 || domains.canFind(email.domainPart), "The email domain `" ~ email.domainPart ~ "` is invalid.");
  }

  /// like validate, but it also checks for the registration process to be enabled
  void validateRegistration(const UserModel user) {
    enforce(isRegistrationEnabled, "The registration process is disabled.");
    validate(user);
  }
}

/// Throw an exception when the email domain is not in the domain list
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains", "value" : "test.com,gmail.com" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  ({
    validator.validate(user);
  }).should.throwAnyException.withMessage("The email domain `yahoo.com` is invalid.");
}

/// should not throw an exception when the email domain is not in the domain list
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains", "value" : "test.com,gmail.com" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = "test";
  user.email = "test@gmail.com";

  validator.validate(user);

  user.email = "test@test.com";
  validator.validate(user);
}

/// Throw no exception when the the domain list is empty
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains", "value" : "" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  validator.validate(user);
}

/// Throw no exception when the the domain list is not set
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  validator.validate(user);
}

/// Throw no exception when the the domain list is undefined
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  validator.validate(user);
}

/// Throw exception when the the first name is empty
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = " ";
  user.username = "test";
  user.email = "test@yahoo.com";

  ({
    validator.validate(user);
  }).should.not.throwAnyException;
}

/// Throw exception when the the last name is empty
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "s";
  user.lastName = " ";
  user.username = "test";
  user.email = "test@yahoo.com";

  ({
    validator.validate(user);
  }).should.not.throwAnyException;
}

/// Throw exception when the the username is empty
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = " ";
  user.email = "test@yahoo.com";

  ({
    validator.validate(user);
  }).should.throwAnyException.withMessage("The provided username is invalid.");
}

/// Throw exception when the the registration is disabled
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "false" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  ({
    validator.validateRegistration(user);
  }).should.throwAnyException.withMessage("The registration process is disabled.");
}

/// it should not throw exception when the the registration is enabled
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled", "value" : "true" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  validator.validateRegistration(user);
}

/// Throw exception when the the registration is not set
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  ({
    validator.validateRegistration(user);
  }).should.throwAnyException.withMessage("The registration process is disabled.");
}

/// it should not throw exception when the the registration is not defined
unittest {
  auto preferences = new MemoryCrate!Preference;
  preferences.addItem(`{"name" : "register.enabled" }`.parseJsonString);
  preferences.addItem(`{"name" : "register.emailDomains" }`.parseJsonString);

  auto validator = new UserDataValidation(preferences);

  UserModel user;
  user.firstName = "test";
  user.lastName = "test";
  user.username = "test";
  user.email = "test@yahoo.com";

  ({
    validator.validateRegistration(user);
  }).should.throwAnyException.withMessage("The registration process is disabled.");
}
